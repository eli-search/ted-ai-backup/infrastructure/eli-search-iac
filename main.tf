# Require TF version to be same as or greater than 0.12.13
terraform {
  required_version = ">=1.3.7"

  # this part needs to be commented for the first initialization (bootstrap)
  # once the bootstrap is done, uncomment it
  /*backend "s3" {
    bucket         = "d-ew1-nlex-terraform"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "d-ew1-nlex-terraform-locks"
    encrypt        = true
  }*/
}

provider "aws" {
  region = var.region
}

/*module "bootstrap" {
  source                        = "./modules/bootstrap"
  terraform_s3_bucket_name      = var.terraform_s3_bucket_name
  terraform_dynamodb_table_name = var.terraform_dynamodb_table_name
}

# All the modules below need to be commented for the first initialization (bootstrap)
# Once the bootstrap is done, uncomment all modules
 module "storage" {
  source                                        = "./modules/storage"
  s3_input_bucket_name                          = var.s3_input_bucket_name
  s3_data_bucket_name                           = var.s3_data_bucket_name
  s3_curated_bucket_name                        = var.s3_curated_bucket_name
  s3_backup_bucket_name                         = var.s3_backup_bucket_name
  s3_deployment_bucket_name                     = var.s3_deployment_bucket_name
  s3_ml_data_bucket_name                        = var.s3_ml_data_bucket_name
  s3_ml_models_bucket_name                      = var.s3_ml_models_bucket_name
  tags                                          = var.tags
  ssm_buckets_input_id                          = var.ssm_buckets_input_id
  ssm_buckets_curated_id                        = var.ssm_buckets_curated_id
  ssm_buckets_ml_data_id                        = var.ssm_buckets_ml_data_id
  ssm_buckets_backup_id                         = var.ssm_buckets_backup_id
  ssm_buckets_deployment_id                     = var.ssm_buckets_deployment_id
  ssm_s3_input_bucket_readwrite_policy_arn      = var.ssm_s3_input_bucket_readwrite_policy_arn
  ssm_s3_curated_bucket_readwrite_policy_arn    = var.ssm_s3_curated_bucket_readwrite_policy_arn
  ssm_s3_curated_bucket_readonly_policy_arn     = var.ssm_s3_curated_bucket_readonly_policy_arn
  ssm_s3_backup_bucket_readonly_policy_arn      = var.ssm_s3_backup_bucket_readonly_policy_arn
  ssm_s3_deployment_bucket_readwrite_policy_arn = var.ssm_s3_deployment_bucket_readwrite_policy_arn
  iam_policy_prefix                             = var.iam_policy_prefix
  ecr_repository_sagemaker_classifiers_name     = var.ecr_repository_sagemaker_classifiers_name
  ecr_repository_application_name               = var.ecr_repository_application_name
  lawfulness_new_notices_queue_arn              = module.lawfulness.new_notices_queue_arn
}

module "classifiers" {
  source                                                         = "./modules/classifiers"
  tags                                                           = var.tags
  sagemaker_classifier_multi_label_division_classifier_image_url = var.sagemaker_classifier_multi_label_division_classifier_image_url
  sagemaker_classifier_multi_label_division_classifier_model_url = var.sagemaker_classifier_multi_label_division_classifier_model_url
  sagemaker_classifier_multi_label_division_classifier_name      = var.sagemaker_classifier_multi_label_division_classifier_name
  sagemaker_classifiers_execution_role_name_prefix               = var.sagemaker_classifiers_execution_role_name_prefix
  ssm_classifier_endpoint_multi_label_division_classifier_name   = var.ssm_classifier_endpoint_multi_label_division_classifier_name
  applications_dashboard_docker_image_url                        = var.applications_dashboard_repository_url
  iam_policy_prefix                                              = var.iam_policy_prefix
  iam_role_prefix                                                = var.iam_role_prefix
  private_subnet_id_list                                         = var.private_subnet_id_list
  dashboard_port                                                 = var.dashboard_port
  vpc_id                                                         = var.vpc_id
  private_subnet_id_az1_list                                     = var.private_subnet_id_az1_list
  private_subnet_id_az2_list                                     = var.private_subnet_id_az2_list
  sagemaker_classifier_budgetary_value_classifier_image_url      = var.sagemaker_classifier_budgetary_value_classifier_image_url
  sagemaker_classifier_budgetary_value_classifier_model_url      = var.sagemaker_classifier_budgetary_value_classifier_model_url
  sagemaker_classifier_budgetary_value_classifier_name           = var.sagemaker_classifier_budgetary_value_classifier_name
  ssm_classifier_endpoint_budgetary_value_classifier_name        = var.ssm_classifier_endpoint_budgetary_value_classifier_name
  sagemaker_classifier_opentender_multi_label_division_classifier_image_url      = var.sagemaker_classifier_opentender_multi_label_division_classifier_image_url
  sagemaker_classifier_opentender_multi_label_division_classifier_model_url      = var.sagemaker_classifier_opentender_multi_label_division_classifier_model_url
  sagemaker_classifier_opentender_multi_label_division_classifier_name           = var.sagemaker_classifier_opentender_multi_label_division_classifier_name
  ssm_classifier_endpoint_opentender_multi_label_division_classifier_name        = var.ssm_classifier_endpoint_opentender_multi_label_division_classifier_name
  project_account_id                                             = var.project_account_id
 lawfulness_host                                                = module.lawfulness.processing_api_host
}

module "ingestion" {
  source                                     = "./modules/ingestion"
  resource_prefix                            = var.ingestion_resource_prefix
  region                                     = var.region
  iam_policy_prefix                          = var.iam_policy_prefix
  iam_role_prefix                            = var.iam_role_prefix
  private_subnet_id_list                     = var.private_subnet_id_list
  application_ingest_resources_url           = var.notice_ingestion_resources_url
  vpc_id                                     = var.vpc_id
  ssm_ecs_ingest_resources_task_definition   = var.ssm_ecs_ingest_resources_task_definition
  ssm_ecs_ingest_resources_cluster_arn       = var.ssm_ecs_ingest_resources_cluster_arn
  ssm_ecs_ingest_resources_container_name    = var.ssm_ecs_ingest_resources_container_name
  ssm_ecs_ingest_resources_security_group_id = var.ssm_ecs_ingest_resources_security_group_id
  project_account_id                         = var.project_account_id
}

module "glue" {
  source             = "./modules/glue"
  account_id         = var.project_account_id
  region             = var.region
  tags               = var.tags
  glue_database_name = var.glue_database_name
  s3_bucket_map      = module.storage.tedai_storage_s3_buckets_map
  s3_policies_map    = module.storage.tedai_storage_s3_policies_map
  iam_role_prefix    = var.iam_role_prefix
  iam_policy_prefix  = var.iam_policy_prefix
}

module "cloudwatch" {
  source                               = "./modules/cloudwatch"
  account_id                           = var.project_account_id
  region                               = var.region
  iam_policy_prefix                    = var.iam_policy_prefix
  ssm_cloudwatch_logs_write_policy_arn = var.ssm_cloudwatch_logs_write_policy_arn
}

module "opensearch" {
  source                 = "./modules/opensearch"
  private_subnet_id_list = var.private_subnet_id_list
  vpc_id                 = var.vpc_id
  tags                   = var.tags
  project_account_id     = var.project_account_id
}

module "db" {
  source                                 = "./modules/db"
  db_identifier                          = var.db_identifier
  db_name                                = var.db_name
  db_instance_class                      = var.db_instance_class
  db_username                            = var.db_username
  db_password_secretsmanager_secret_name = var.db_password_secretsmanager_secret_name
  vpc_id                                 = var.vpc_id
  vpc_cidr                               = var.vpc_cidr
  ssm_prefix                             = var.ssm_prefix
  private_subnet_id_list                 = var.private_subnet_id_list
  tags                                   = var.tags
}

module "lawfulness" {
  source                         = "./modules/lawfulness"
  region                         = var.region
  tags                           = var.tags
  vpc_id                         = var.vpc_id
  private_subnet_id_list         = var.private_subnet_id_list
  private_subnet_id_az1_list     = var.private_subnet_id_az1_list
  private_subnet_id_az2_list     = var.private_subnet_id_az2_list
  iam_policy_prefix              = var.iam_policy_prefix
  iam_role_prefix                = var.iam_role_prefix
  ssm_prefix                     = var.ssm_prefix
  resource_prefix                = var.lawfulness_resource_prefix
  task_image                     = var.lawfulness_task_image
  input_bucket_arn               = module.storage.tedai_storage_s3_buckets_map.input_bucket.arn
  dashboard_security_group_id    = module.classifiers.dashboard_security_group_id
  db_name                        = var.db_name
  db_username                    = var.db_username
  db_host                        = module.db.host
  db_port                        = module.db.port
  db_password_ssm_parameter_arn  = module.db.password_ssm_parameter_arn
  db_password_ssm_parameter_name = module.db.password_ssm_parameter_name
  api_port                       = var.lawfulness_api_port
  thread_count                   = var.lawfulness_thread_count
}

module "extraction" {
  source                                           = "./modules/extraction"
  project_account_id                               = var.project_account_id
  region                                           = var.region
  resource_prefix                                  = var.notice_data_extraction_prefix
  iam_policy_prefix                                = var.iam_policy_prefix
  iam_role_prefix                                  = var.iam_role_prefix
  input_bucket_arn                                 = module.storage.tedai_storage_s3_buckets_map.input_bucket.arn
  application_notice_data_extraction_url           = var.notice_data_extraction_resources_url
  private_subnet_id_list                           = var.private_subnet_id_list
  vpc_id                                           = var.vpc_id
  ssm_ecs_notice_data_extraction_task_definition   = var.ssm_ecs_notice_data_extraction_task_definition
  ssm_ecs_notice_data_extraction_cluster_arn       = var.ssm_ecs_notice_data_extraction_cluster_arn
  ssm_ecs_notice_data_extraction_container_name    = var.ssm_ecs_notice_data_extraction_container_name
  ssm_ecs_notice_data_extraction_security_group_id = var.ssm_ecs_notice_data_extraction_security_group_id
}*/