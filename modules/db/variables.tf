variable "tags" {
  type = map(string)
}

variable "ssm_prefix" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "db_identifier" {
  type = string
}

variable "db_password_secretsmanager_secret_name" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_instance_class" {
  type = string
}

variable "db_username" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr" {
  type = string
}