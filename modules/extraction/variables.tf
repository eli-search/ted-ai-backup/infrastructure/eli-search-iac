variable "project_account_id" {
  type = string
}

variable "region" {
  type = string
}

variable "resource_prefix" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

variable "iam_role_prefix" {
  type = string
}

variable "input_bucket_arn" {
  type = string
}

variable "application_notice_data_extraction_url" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "vpc_id" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_task_definition" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_cluster_arn" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_container_name" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_security_group_id" {
  type = string
}