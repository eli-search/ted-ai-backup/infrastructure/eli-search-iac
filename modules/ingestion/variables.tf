variable "resource_prefix" {
  type = string
}

variable "region" {
  type = string
}

variable "application_ingest_resources_url" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

# IAM
variable "iam_role_prefix" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

# Network
variable "vpc_id" {
  type = string
}

# SSM
variable "ssm_ecs_ingest_resources_task_definition" {
  type = string
}

variable "ssm_ecs_ingest_resources_cluster_arn" {
  type = string
}

variable "ssm_ecs_ingest_resources_container_name" {
  type = string
}

variable "ssm_ecs_ingest_resources_security_group_id" {
  type = string
}

variable "project_account_id" {
  type = string
}