variable "vpc_id" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "tags" {
  type = map(string)
}

variable "project_account_id" {
  type = string
}
