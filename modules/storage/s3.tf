/*locals {
  tedai_storage_s3_buckets_map = {
    input_bucket = {
      arn   = aws_s3_bucket.input_bucket.arn
      name  = aws_s3_bucket.input_bucket.id
      s3url = "s3://${aws_s3_bucket.input_bucket.id}/"
    }
    data_bucket = {
      arn   = aws_s3_bucket.data_bucket.arn
      name  = aws_s3_bucket.data_bucket.id
      s3url = "s3://${aws_s3_bucket.data_bucket.id}/"
    }
    curated_bucket = {
      arn   = aws_s3_bucket.curated_bucket.arn
      name  = aws_s3_bucket.curated_bucket.id
      s3url = "s3://${aws_s3_bucket.curated_bucket.id}/"
    }
    backup_bucket = {
      arn   = aws_s3_bucket.backup_bucket.arn
      name  = aws_s3_bucket.backup_bucket.id
      s3url = "s3://${aws_s3_bucket.backup_bucket.id}/"
    }
    deployment_bucket = {
      arn   = aws_s3_bucket.deployment_bucket.arn
      name  = aws_s3_bucket.deployment_bucket.id
      s3url = "s3://${aws_s3_bucket.deployment_bucket.id}/"
    }
  }
  tedai_storage_s3_policies_map = {
    input_readwrite_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_input_readwrite.arn
      name = aws_iam_policy.storage_s3_bucket_input_readwrite.name
    }
    input_readonly_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_input_readonly.arn
      name = aws_iam_policy.storage_s3_bucket_input_readonly.name
    }
    curated_readwrite_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_curated_readwrite.arn
      name = aws_iam_policy.storage_s3_bucket_curated_readwrite.name
    }
    curated_readonly_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_curated_readonly.arn
      name = aws_iam_policy.storage_s3_bucket_curated_readonly.name
    }
    backup_readonly_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_backup_readonly.arn
      name = aws_iam_policy.storage_s3_bucket_backup_readonly.name
    }
    deployment_readwrite_policy = {
      arn  = aws_iam_policy.storage_s3_bucket_curated_readwrite.arn
      name = aws_iam_policy.storage_s3_bucket_curated_readwrite.name
    }
  }
}


resource "aws_s3_bucket" "input_bucket" {
  bucket = var.s3_input_bucket_name

  # Prevents Terraform from destroying or replacing this object - a great safety mechanism
  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "input_bucket" {
  bucket = aws_s3_bucket.input_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "input_bucket" {
  bucket     = aws_s3_bucket.input_bucket.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.curated_bucket]
}*/

/*resource "aws_s3_bucket_ownership_controls" "input_bucket" {
  bucket = aws_s3_bucket.input_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "input_bucket" {
  bucket = aws_s3_bucket.input_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "input_bucket" {
  bucket = aws_s3_bucket.input_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_ssm_parameter" "input_bucket" {
  name  = var.ssm_buckets_input_id
  type  = "String"
  value = aws_s3_bucket.input_bucket.id
}

resource "aws_s3_bucket_notification" "input_bucket" {
  bucket = aws_s3_bucket.input_bucket.id

  queue {
    queue_arn     = var.lawfulness_new_notices_queue_arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".xml"
  }
}

resource "aws_s3_bucket" "data_bucket" {
  bucket = var.s3_data_bucket_name

  # Prevents Terraform from destroying or replacing this object - a great safety mechanism
  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "data_bucket" {
  bucket = aws_s3_bucket.data_bucket.id
  versioning_configuration {
    status = "Suspended"
  }
}*/
/*
resource "aws_s3_bucket_acl" "data_bucket" {
  bucket     = aws_s3_bucket.data_bucket.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.curated_bucket]
}*/

/*resource "aws_s3_bucket_ownership_controls" "data_bucket" {
  bucket = aws_s3_bucket.data_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "data_bucket" {
  bucket = aws_s3_bucket.data_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "data_bucket" {
  bucket = aws_s3_bucket.data_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket" "ml_data_bucket" {
  bucket = var.s3_ml_data_bucket_name

  # Prevents Terraform from destroying or replacing this object - a great safety mechanism
  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "ml_data_bucket" {
  bucket = aws_s3_bucket.ml_data_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "ml_data_bucket" {
  bucket = aws_s3_bucket.ml_data_bucket.id
  acl    = "private"
}*/

/*resource "aws_s3_bucket_public_access_block" "ml_data_bucket" {
  bucket = aws_s3_bucket.ml_data_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "ml_data_bucket" {
  bucket = aws_s3_bucket.ml_data_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_ssm_parameter" "ml_data_bucket" {
  name  = var.ssm_buckets_ml_data_id
  type  = "String"
  value = aws_s3_bucket.ml_data_bucket.id
}

resource "aws_s3_bucket" "ml_models_bucket" {
  bucket = var.s3_ml_models_bucket_name

  # Prevents Terraform from destroying or replacing this object - a great safety mechanism
  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "ml_models_bucket" {
  bucket = aws_s3_bucket.ml_models_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "ml_models_bucket" {
  bucket = aws_s3_bucket.ml_models_bucket.id
  acl    = "private"
}*/

/*resource "aws_s3_bucket_public_access_block" "ml_models_bucket" {
  bucket = aws_s3_bucket.ml_models_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "ml_models_bucket" {
  bucket = aws_s3_bucket.ml_models_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_iam_policy" "storage_s3_bucket_input_readwrite" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_INPUT_READWRITE_POLICY"
  description = "Read/Write access to bucket input by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_input_readwrite.json
}

data "aws_iam_policy_document" "storage_s3_bucket_input_readwrite" {
  statement {
    sid = "S3ReadWriteInput"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*",
      "s3:PutObject",
      "s3:DeleteObject"
    ]

    resources = ["${aws_s3_bucket.input_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketInput"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.input_bucket.arn]
  }
}

resource "aws_ssm_parameter" "input_bucket_readwrite_policy" {
  name  = var.ssm_s3_input_bucket_readwrite_policy_arn
  type  = "String"
  value = aws_iam_policy.storage_s3_bucket_input_readwrite.arn
}

resource "aws_iam_policy" "storage_s3_bucket_input_readonly" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_INPUT_READONLY_POLICY"
  description = "Read access to bucket input by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_input_readonly.json
}

data "aws_iam_policy_document" "storage_s3_bucket_input_readonly" {
  statement {
    sid = "S3ReadOnlyInput"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*"
    ]

    resources = ["${aws_s3_bucket.input_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketInput"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.input_bucket.arn]
  }
}

resource "aws_s3_bucket" "curated_bucket" {
  bucket = var.s3_curated_bucket_name

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "curated_bucket" {
  bucket = aws_s3_bucket.curated_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "curated_bucket" {
  bucket     = aws_s3_bucket.curated_bucket.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.curated_bucket]
}*/

/*resource "aws_s3_bucket_ownership_controls" "curated_bucket" {
  bucket = aws_s3_bucket.curated_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "curated_bucket" {
  bucket = aws_s3_bucket.curated_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "curated_bucket" {
  bucket = aws_s3_bucket.curated_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_ssm_parameter" "curated_bucket" {
  name  = var.ssm_buckets_curated_id
  type  = "String"
  value = aws_s3_bucket.curated_bucket.id
}

resource "aws_iam_policy" "storage_s3_bucket_curated_readwrite" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_CURATED_READWRITE_POLICY"
  description = "Read/Write access to bucket curated by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_curated_readwrite.json
}

data "aws_iam_policy_document" "storage_s3_bucket_curated_readwrite" {
  statement {
    sid = "S3ReadWriteCurated"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*",
      "s3:PutObject",
      "s3:DeleteObject"
    ]

    resources = ["${aws_s3_bucket.curated_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketCurated"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.curated_bucket.arn]
  }
}

resource "aws_ssm_parameter" "curated_bucket_readwrite_policy" {
  name  = var.ssm_s3_curated_bucket_readwrite_policy_arn
  type  = "String"
  value = aws_iam_policy.storage_s3_bucket_curated_readwrite.arn
}

resource "aws_iam_policy" "storage_s3_bucket_curated_readonly" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_CURATED_READONLY_POLICY"
  description = "Read access to bucket curated by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_curated_readonly.json
}

data "aws_iam_policy_document" "storage_s3_bucket_curated_readonly" {
  statement {
    sid = "S3ReadOnlyCurated"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*"
    ]

    resources = ["${aws_s3_bucket.curated_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketCurated"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.curated_bucket.arn]
  }
}

resource "aws_ssm_parameter" "curated_bucket_readonly_policy" {
  name  = var.ssm_s3_curated_bucket_readonly_policy_arn
  type  = "String"
  value = aws_iam_policy.storage_s3_bucket_curated_readonly.arn
}

resource "aws_s3_bucket" "backup_bucket" {
  bucket = var.s3_backup_bucket_name

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "backup_bucket" {
  bucket = aws_s3_bucket.backup_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "backup_bucket" {
  bucket     = aws_s3_bucket.backup_bucket.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.backup_bucket]
}

resource "aws_s3_bucket_ownership_controls" "backup_bucket" {
  bucket = aws_s3_bucket.backup_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}*/

/*resource "aws_s3_bucket_public_access_block" "backup_bucket" {
  bucket = aws_s3_bucket.input_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "backup_bucket" {
  bucket = aws_s3_bucket.backup_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_ssm_parameter" "backup_bucket" {
  name  = var.ssm_buckets_backup_id
  type  = "String"
  value = aws_s3_bucket.backup_bucket.id
}

resource "aws_iam_policy" "storage_s3_bucket_backup_readonly" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_BACKUP_READONLY_POLICY"
  description = "Read access to bucket backup by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_backup_readonly.json
}

data "aws_iam_policy_document" "storage_s3_bucket_backup_readonly" {
  statement {
    sid = "S3ReadOnlyBackup"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*"
    ]

    resources = ["${aws_s3_bucket.backup_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketBackup"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.backup_bucket.arn]
  }
}

resource "aws_ssm_parameter" "backup_bucket_readonly_policy" {
  name  = var.ssm_s3_backup_bucket_readonly_policy_arn
  type  = "String"
  value = aws_iam_policy.storage_s3_bucket_backup_readonly.arn
}


resource "aws_s3_bucket" "deployment_bucket" {
  bucket = var.s3_deployment_bucket_name

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_versioning" "deployment_bucket" {
  bucket = aws_s3_bucket.deployment_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}*/
/*
resource "aws_s3_bucket_acl" "deployment_bucket" {
  bucket     = aws_s3_bucket.deployment_bucket.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.deployment_bucket]
}*/

/*resource "aws_s3_bucket_ownership_controls" "deployment_bucket" {
  bucket = aws_s3_bucket.deployment_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "deployment_bucket" {
  bucket = aws_s3_bucket.deployment_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "deployment_bucket" {
  bucket = aws_s3_bucket.deployment_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_ssm_parameter" "deployment_bucket" {
  name  = var.ssm_buckets_deployment_id
  type  = "String"
  value = aws_s3_bucket.deployment_bucket.id
}

resource "aws_iam_policy" "storage_s3_bucket_deployment_readwrite" {
  name        = "${var.iam_policy_prefix}_S3_STORAGE_DEPLOYMENT_READWRITE_POLICY"
  description = "Read/Write access to bucket deployment by application"
  policy      = data.aws_iam_policy_document.storage_s3_bucket_deployment_readwrite.json
}

data "aws_iam_policy_document" "storage_s3_bucket_deployment_readwrite" {
  statement {
    sid = "S3ReadWriteCurated"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:Describe*",
      "s3:Head*",
      "s3:PutObject",
      "s3:DeleteObject"
    ]

    resources = ["${aws_s3_bucket.deployment_bucket.arn}/*"]
  }
  statement {
    sid = "S3ListBucketCurated"
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]

    resources = [aws_s3_bucket.deployment_bucket.arn]
  }
}

resource "aws_ssm_parameter" "deployment_bucket_readwrite_policy" {
  name  = var.ssm_s3_deployment_bucket_readwrite_policy_arn
  type  = "String"
  value = aws_iam_policy.storage_s3_bucket_deployment_readwrite.arn
}*/
