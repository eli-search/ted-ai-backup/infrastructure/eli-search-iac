/*resource "aws_glue_catalog_database" "aws_glue_catalog_database" {
  name = var.glue_database_name
}

resource "aws_glue_catalog_table" "glue_curated_notice_data_table" {
  name          = "d_ew1_ted_ai_curated_notice_data"
  database_name = var.glue_database_name

  table_type = "EXTERNAL_TABLE"

  parameters = {
    EXTERNAL              = "TRUE"
    "parquet.compression" = "UNCOMPRESSED"
  }

  storage_descriptor {
    location      = "s3://${var.s3_bucket_map["curated_bucket"].name}/data_source=notice/format=parquet"
    input_format  = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat"

    ser_de_info {
      name                  = "curated-notice-data-parquet-stream"
      serialization_library = "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"

      parameters = {
        "serialization.format" = 1
      }
    }

    columns {
      name = "id"
      type = "string"
    }

    columns {
      name = "type"
      type = "string"
    }

    columns {
      name = "title"
      type = "string"
    }

    columns {
      name = "short_description"
      type = "string"
    }

    columns {
      name = "contract_type"
      type = "string"
    }

    columns {
      name = "main_cpv"
      type = "string"
    }

    columns {
      name = "additional_cpvs"
      type = "string"
    }

    columns {
      name = "publication_date"
      type = "string"
    }

    columns {
      name = "reference_notice_id"
      type = "string"
    }

    columns {
      name = "version"
      type = "string"
    }

    columns {
      name = "etendering_url"
      type = "string"
    }

    columns {
      name = "awarded_companies"
      type = "string"
    }

    columns {
      name = "budgetary_values"
      type = "string"
    }

    columns {
      name = "buyer"
      type = "string"
    }

    columns {
      name = "country"
      type = "string"
    }

    columns {
      name = "language"
      type = "string"
    }

    columns {
      name = "is_eu_institution"
      type = "boolean"
    }

    columns {
      name = "year"
      type = "string"
    }

    columns {
      name = "month"
      type = "string"
    }

    columns {
      name = "day"
      type = "string"
    }

    columns {
      name = "contract_id"
      type = "string"
    }

    columns {
      name = "ingestion_type"
      type = "string"
    }

    columns {
      name = "s3_bucket"
      type = "string"
    }

    columns {
      name = "s3_key_path"
      type = "string"
    }
  }
}

resource "aws_iam_role" "curated_data_crawler_role" {
  name               = "${var.iam_role_prefix}_GLUE_CRAWLER_CURATED_ROLE"
  description        = "Execution Role for Glue crawler on curated bucket"
  assume_role_policy = data.aws_iam_policy_document.assume_glue_role.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole",
    var.s3_policies_map["curated_readonly_policy"].arn
  ]
  permissions_boundary = "arn:aws:iam::${var.account_id}:policy/Team_Admin_Boundary"
  tags                 = var.tags
}

data "aws_iam_policy_document" "assume_glue_role" {
  statement {
    sid     = "StsAssumeGlueRole"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["glue.amazonaws.com"]
    }
  }
}*/
