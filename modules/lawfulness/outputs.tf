output "new_notices_queue_arn" {
  value = aws_sqs_queue.new_notices.arn
}

output "processing_api_host" {
  value = aws_alb.processing_api.dns_name
}
