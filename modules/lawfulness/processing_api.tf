/*resource "aws_iam_policy" "processing_api" {
  name = "${var.iam_policy_prefix}_LAWFULNESS_PROCESSING_API_POLICY"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["ssm:GetParameter"]
        Effect   = "Allow"
        Resource = var.db_password_ssm_parameter_arn
      },
      {
        Action   = ["logs:CreateLogStream", "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "${aws_cloudwatch_log_group.cluster.arn}:*"
      },
      {
        Action   = ["ecr:BatchGetImage*", "ecr:BatchCheck*", "ecr:Get*", "ecr:List*", "ecr:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "processing_api" {
  name                 = "${var.iam_role_prefix}_LAWFULNESS_PROCESSING_API_ROLE"
  assume_role_policy   = data.aws_iam_policy_document.ecs_assume_role_policy.json
  managed_policy_arns  = [aws_iam_policy.processing_api.arn]
  permissions_boundary = "arn:aws:iam::195848431569:policy/Team_Admin_Boundary"
}


resource "aws_ecs_task_definition" "processing_api" {
  family                   = "${var.resource_prefix}-processing-api"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = aws_iam_role.processing_api.arn
  task_role_arn            = aws_iam_role.processing_api.arn
  container_definitions = jsonencode([
    {
      name               = "lawfulness"
      image              = var.task_image
      cpu                = 1024
      memory             = 2048
      execution_role_arn = aws_iam_role.processing_api.arn
      task_role_arn      = aws_iam_role.processing_api.arn
      essential          = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cluster.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "ecs"
        }
      }
      portMappings = [
        {
          containerPort = var.api_port,
          hostPort      = var.api_port
        }
      ]
      healthCheck = {
        command     = ["CMD-SHELL", "curl -f http://localhost:${var.api_port}/health || exit 1"]
        startPeriod = 10
      }
      environment = concat(local.task_environment, [{ name = "MODE", value = "api" }])
    }
  ])
}*/

/*resource "aws_security_group" "processing_api" {
  name        = "${var.resource_prefix}-processing-api"
  description = "Allow internet and service port access in lawfulness processing API"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = var.api_port
    to_port         = var.api_port
    security_groups = [aws_security_group.processing_api_load_balancer.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}*/

/*resource "aws_ecs_service" "processing_api" {
  name            = "${var.resource_prefix}-processing-api"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.processing_api.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.processing_api.id]
    subnets          = var.private_subnet_id_list
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.processing_api.id
    container_name   = "lawfulness"
    container_port   = var.api_port
  }
  depends_on = [aws_iam_role.processing_api, aws_alb.processing_api]
}*/

/*resource "aws_security_group" "processing_api_load_balancer" {
  name        = "${var.resource_prefix}-processing-api-load-balancer"
  description = "Controls access to the ALB of lawfulness API"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [var.dashboard_security_group_id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "random_shuffle" "az1" {
  input        = var.private_subnet_id_az1_list
  result_count = 1
}

resource "random_shuffle" "az2" {
  input        = var.private_subnet_id_az2_list
  result_count = 1
}

resource "aws_alb" "processing_api" {
  name            = "lawfulness-processing-api"
  subnets         = concat(random_shuffle.az1.result, random_shuffle.az2.result)
  security_groups = [aws_security_group.processing_api_load_balancer.id]
  internal        = true
}

resource "aws_alb_target_group" "processing_api" {
  name        = "lawfulness-processing-api"
  port        = var.api_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/health"
    unhealthy_threshold = "3"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "processing_api" {
  load_balancer_arn = aws_alb.processing_api.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.processing_api.id
    type             = "forward"
  }
}*/
