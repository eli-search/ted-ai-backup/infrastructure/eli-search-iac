/*resource "aws_ecs_cluster" "cluster" {
  name = var.resource_prefix

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = aws_cloudwatch_log_group.cluster.name
      }
    }
  }
}

resource "aws_cloudwatch_log_group" "cluster" {
  name = "/tedai/lawfulness"
}

resource "aws_ecs_cluster_capacity_providers" "cluster" {
  cluster_name       = aws_ecs_cluster.cluster.name
  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}

locals {
  task_environment = [
    {
      name  = "LOG_LEVEL"
      value = "DEBUG"
    },
    {
      name  = "THREAD_COUNT"
      value = tostring(var.thread_count)
    },
    {
      name  = "FLAGGED_NOTICES_DYNAMODB_TABLE_NAME"
      value = aws_dynamodb_table.flagged_notices.name
    },
    {
      name  = "MAX_WHITELISTED_OFFICIAL_NAME_SIMILARITY"
      value = "0.8"
    },
    {
      name  = "DB_HOST"
      value = var.db_host
    },
    {
      name  = "DB_PORT"
      value = var.db_port
    },
    {
      name  = "DB_NAME"
      value = var.db_name
    },
    {
      name  = "DB_USERNAME"
      value = var.db_username
    },
    {
      name  = "DB_PASSWORD_SSM_PARAMETER"
      value = var.db_password_ssm_parameter_name
    },
    {
      name  = "DB_WHITELISTED_BODIES_TABLE"
      value = "whitelisted_contracting_bodies"
    },
    {
      name  = "NEW_NOTICES_QUEUE_URL"
      value = aws_sqs_queue.new_notices.url
    },
    {
      name  = "NEW_NOTICES_BATCH_SIZE"
      value = "10"
    },
  ]
}*/
