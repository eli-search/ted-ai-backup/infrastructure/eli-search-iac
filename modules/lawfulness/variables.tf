variable "region" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "vpc_id" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "private_subnet_id_az1_list" {
  type = list(string)
}

variable "private_subnet_id_az2_list" {
  type = list(string)
}

variable "iam_policy_prefix" {
  type = string
}

variable "iam_role_prefix" {
  type = string
}

variable "ssm_prefix" {
  type = string
}

variable "resource_prefix" {
  type = string
}

variable "task_image" {
  type = string
}

variable "input_bucket_arn" {
  type = string
}

variable "dashboard_security_group_id" {
  type = string
}

variable "db_host" {
  type = string
}

variable "db_port" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_username" {
  type = string
}

variable "db_password_ssm_parameter_name" {
  type = string
}

variable "db_password_ssm_parameter_arn" {
  type = string
}


variable "api_port" {
  type = number
}

variable "thread_count" {
  type = number
}

